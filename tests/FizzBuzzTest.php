<?php

use PHPUnit\Framework\TestCase;
use BWB\Framework\mvc\controllers\FizzBuzzController;

class FizzBuzzTest extends TestCase
{
    private $controller;
    public $value;

    public function setUp() : void
    {
        $this->controller = new FizzBuzzController();
        $this->value = 22;
    }

    public function testFizzBuzzArgNotNum()
    {
       $result = $this->controller->fizzBuzzing("plop");
       $this->assertFalse($result); 
    }

    public function testFizzBuzzIf3()
    {
       $result = $this->controller->fizzBuzzing(3);
       $this->assertEquals("fizz", $result); 
    }

    public function testFizzBuzzIf5()
    {
       $result = $this->controller->fizzBuzzing(5);
       $this->assertEquals("buzz", $result); 
    }

    public function testFizzBuzzIf3And5()
    {
       $result = $this->controller->fizzBuzzing(15);
       $this->assertEquals("fizzbuzz", $result); 
    }

    public function testFizzBuzzIfNot3Or5()
    {
       $result = $this->controller->fizzBuzzing($this->value);
       $this->assertEquals("perdu", $result); 
    }
}