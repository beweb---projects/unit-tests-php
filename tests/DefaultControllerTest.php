<?php

use PHPUnit\Framework\TestCase;
use BWB\Framework\mvc\controllers\DefaultController;
use BWB\Framework\mvc\dao\DAODefault;

class DefaultControllerTest extends TestCase
{
    private $controller;
    public $value1;
    public $value2;

    public function setUp() : void
    {
        $this->controller = new DAODefault();
        $this->value1 = 24;
        $this->value2 = 10;
    }

    // /**
    //  * Test Soustraction
    //  */

    // public function testSoustractionIsOk()
    // {
    //     $result = $this->controller->soustraction($this->value1, $this->value2);
    //     $this->assertEquals(14, $result);
    // }

    // public function testSoustractionIsNotOk()
    // {
    //     $result = $this->controller->soustraction($this->value1, $this->value2);
    //     $this->assertNotEquals(4, $result);
    // }

    // public function testSoustractionOneString()
    // {
    //     $result = $this->controller->soustraction($this->value1, "paul");
    //     $this->assertFalse($result);
    // }

    // public function testSoustractionAllString()
    // {
    //     $result = $this->controller->soustraction("pierre", "paul");
    //     $this->assertFalse($result);
    // }

    // public function testSoustractionResultNeg()
    // {
    //     $result = $this->controller->soustraction($this->value2, $this->value1);
    //     $this->assertEquals(0, $result);
    // }

    // /**
    //  * Test Divisions
    //  */

    // public function testDivisionIsOk()
    // {
    //     $result = $this->controller->division($this->value1, $this->value2);
    //     $this->assertEquals(14, $result);
    // }

    // public function testDivisionIsNotOk()
    // {
    //     $result = $this->controller->division($this->value1, $this->value2);
    //     $this->assertNotEquals(4, $result);
    // }

    // public function testDivisionOneString()
    // {
    //     $result = $this->controller->division($this->value1, "paul");
    //     $this->assertFalse($result);
    // }

    // public function testDivisionAllString()
    // {
    //     $result = $this->controller->division("pierre", "paul");
    //     $this->assertFalse($result);
    // }

    // public function testDivisionResultNeg()
    // {
    //     $result = $this->controller->division($this->value2, $this->value1);
    //     $this->assertEquals(0, $result);
    // }

    /**
     * Test Retrieve
     */

    public function testRetrieveArgNotInt()
    {
        $result = $this->controller->retrieve("pof");
        $this->assertFalse($result);
    }

    public function testRetrieveReturnNull()
    {
        $result = $this->controller->retrieve(500);
        $this->assertNull($result);
    }

    public function testRetrieveReturnArray()
    {
        $result = $this->controller->retrieve($this->value1);
        $this->assertIsArray($result);
    }
}