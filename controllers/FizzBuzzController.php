<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\models\DefaultModel;

class FizzBuzzController extends Controller
{
    public function fizzBuzzing($nombre)
    {
        if(!is_numeric($nombre)) return false;

        if($nombre%3 == 0 && $nombre%5 == 0) return "fizzbuzz";

        if($nombre%3 == 0) return "fizz";

        if($nombre%5 == 0) return "buzz";

        return "perdu";
    }
}